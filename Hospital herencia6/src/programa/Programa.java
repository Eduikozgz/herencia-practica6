package programa;

import clases.GestorMedicacion;

public class Programa {

	public static void main(String[] args) {
		
		
		System.out.println("1");
		System.out.println("Creamos el gestor");
		GestorMedicacion gestor = new GestorMedicacion();
		System.out.println("2");
		System.out.println("Damos de alta pacientes");
		gestor.altaPaciente("Coronavirus", "Kim");
		gestor.altaPaciente("Cancer pulmon", "Fernando");
		gestor.altaPaciente("Neumonia", "Manolo");
		System.out.println("3");
		System.out.println("Listamos los responsables");
		gestor.listarPacientes();
		System.out.println("4");
		System.out.println("Buscamos paciente 222222");
		System.out.println(gestor.buscarPaciente("222222"));
		System.out.println("5");
		System.out.println("Damos de alta medicacion");
		gestor.altaMedicacion("Vacuna" , "codigo1" , 15.5 , "2020-04-24");
		gestor.altaMedicacion("Quimio" , "codigo2" , 30.7 , "2018-09-24");
		gestor.altaMedicacion("Antibioticos" , "codigo3" , 5.0 , "2014-01-17");
		System.out.println("Listamos trabajos");
		gestor.listarMedicacion();
		System.out.println("6");
		System.out.println("Asignar pacientes");
		gestor.asignarPaciente("Epilepsia", "Sylvia");
		gestor.asignarPaciente("Disfagia", "Mario");
		System.out.println("7");
		System.out.println("Mostrar medicacion de paciente 111111");
		gestor.listarMedicacionDePacientes("111111");
		System.out.println("Mostrar medicacion de paciente 222222");
		gestor.listarMedicacionDePacientes("222222");
		System.out.println("8");
		System.out.println("Mostrar medicacion por a�o");
		gestor.listarMedicacionAnio(2018);
		System.out.println("9");
		System.out.println("Eliminar Medicacion medicacion1");
		gestor.eliminarMedicacion("medicacion1");
		System.out.println("Listamos medicamentos");
		gestor.listarMedicacion();
		System.out.println("10");
		System.out.println("Listamos medicacion de paciente 111111");
		gestor.listarMedicacionDePacientes("111111");
		System.out.println("Listamos medicacion de paciente 222222");
		gestor.listarMedicacionDePacientes("222222");
		System.out.println("11");
		System.out.println("Listar Pacientes  antes del ere");
		gestor.listarPacientes();
		gestor.ere();
		System.out.println("Listar Pacientes  despues del ere");
		gestor.listarPacientes();
	}

}
