package clases;

import java.time.LocalDate;

public class Paciente {

	private String enfermedad;
	private String nombre;
	private LocalDate fechaIngreso;

	public Paciente(String enfermedad, String nombre) {

		this.enfermedad = enfermedad;
		this.nombre = nombre;

	}
	
	// Setters Y Getters

	public String getEnfermedad() {
		return enfermedad;
	}

	public void setEnfermedad(String enfermedad) {
		this.enfermedad = enfermedad;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LocalDate getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(LocalDate fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public String toString() {
		return "Paciente [enfermedad=" + enfermedad + ", nombre=" + nombre + ", fechaIngreso=" + fechaIngreso + "]";
	}

}
