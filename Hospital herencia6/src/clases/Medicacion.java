package clases;

import java.time.LocalDate;

public class Medicacion {
	
	private String nombre;
	private String codigo;
	private Double valor;
	private LocalDate fechaCreacion;
	private Paciente pacienteMedicacion;
	
	
	public Medicacion(String nombre ,  String codigo ,Double valor) {
		
		this.nombre = nombre;
		this.codigo = codigo;
		this.valor = valor;
		
	}
	
	
	// Setters Y Getters
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public Double getValor() {
		return valor;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public LocalDate getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(LocalDate fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public Paciente getPacienteMedicacion() {
		return pacienteMedicacion;
	}
	public void setPacienteMedicacion(Paciente pacienteMedicacion) {
		this.pacienteMedicacion = pacienteMedicacion;
	}
	
	//ToString
	
	public String toString() {
		return "Medicacion [nombre=" + nombre + ", codigo=" + codigo + ", valor=" + valor + ", fechaCreacion="
				+ fechaCreacion + ", pacienteMedicacion=" + pacienteMedicacion + "]";
	}


}
