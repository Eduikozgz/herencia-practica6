package clases;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;

public class GestorMedicacion {

	private ArrayList<Paciente> listaPacientes;
	private ArrayList<Medicacion> listaMedicacion;

	public GestorMedicacion() {

		listaPacientes = new ArrayList<Paciente>();
		listaMedicacion = new ArrayList<Medicacion>();

	}

	public void altaPaciente(String enfermedad, String nombre) {

		if (!existePacientes(enfermedad)) {

			Paciente nuevoPaciente = new Paciente(enfermedad, nombre);
			nuevoPaciente.setFechaIngreso(LocalDate.now());
			listaPacientes.add(nuevoPaciente);

		} else {
			System.out.println("El Paciente ya existe ");
		}

	}

	public boolean existePacientes(String enfermedad) {

		for (Paciente paciente : listaPacientes) {

			if (paciente != null && paciente.getEnfermedad().equals(enfermedad)) {
				return true;
			}

		}
		return false;
	}

	public void listarPacientes() {

		for (Paciente paciente : listaPacientes) {

			if (paciente != null) {
				System.out.println(paciente);
			}

		}

	}
	
	
	public void listarMedicacion() {

		for (Medicacion medicacion : listaMedicacion) {

			if (medicacion != null) {
				System.out.println(medicacion);
			}

		}

	}

	public Paciente buscarPaciente(String Enfermedad) {

		for (Paciente paciente : listaPacientes) {
			if (paciente != null && paciente.getEnfermedad().equals(Enfermedad)) {
				return paciente;

			}
		}
		return null;

	}

	public void altaMedicacion(String nombre, String codigo, Double valor, String fechaCreacion) {

		Medicacion nuevaMedicacion = new Medicacion(nombre, codigo, valor);
		nuevaMedicacion.setFechaCreacion(LocalDate.parse(fechaCreacion));
		listaMedicacion.add(nuevaMedicacion);

	}

	public void eliminarMedicacion(String nombre) {

		Iterator<Medicacion> iteradorMedicacion = listaMedicacion.iterator();

		while (iteradorMedicacion.hasNext()) {

			Medicacion medicacion = iteradorMedicacion.next();

			if (medicacion.getNombre().equals(nombre)) {

				iteradorMedicacion.remove();

			}

		}

	}

	public void asignarPaciente(String enfermedad, String nombreMedicacion) {

		if (buscarPaciente(enfermedad) != null && buscarMedicacion(nombreMedicacion) != null) {
			Paciente paciente = buscarPaciente(enfermedad);
			Medicacion medicacion = buscarMedicacion(nombreMedicacion);
			medicacion.setPacienteMedicacion(paciente);
		}

	}

	public Medicacion buscarMedicacion(String nombreMedicacion) {

		for (Medicacion medicacion : listaMedicacion) {
			if (medicacion != null && medicacion.getNombre().equals(nombreMedicacion)) {

				return medicacion;

			}
		}
		return null;

	}

	public void listarMedicacionAnio(int anio) {

		for (Medicacion medicacion : listaMedicacion) {

			if (medicacion.getFechaCreacion().getYear() == anio) {

				System.out.println(medicacion);

			}

		}

	}

	public void listarMedicacionDePacientes(String enfermedad) {

		for (Medicacion medicacion : listaMedicacion) {

			if (medicacion.getPacienteMedicacion() != null
					&& medicacion.getPacienteMedicacion().getEnfermedad().equals(enfermedad)) {
				System.out.println(medicacion);
			}

		}

	}

	public void asignarPacienteContinuo(String nombreMedicacion) {

		Medicacion medicacion = buscarMedicacion(nombreMedicacion);

		if (medicacion != null) {

			medicacion.setPacienteMedicacion(buscarPacienteContinuo());
		}

	}

	public Paciente buscarPacienteContinuo() {

		LocalDate fechaAntigua = null;
		for (int i = 0; i < listaPacientes.size(); i++) {
			Paciente pacienteActual = listaPacientes.get(i);
			if (pacienteActual != null && i == 0) {

				fechaAntigua = pacienteActual.getFechaIngreso();

			} else {
				if (pacienteActual != null && pacienteActual.getFechaIngreso().isAfter(fechaAntigua)) {
					fechaAntigua = pacienteActual.getFechaIngreso();
				}
			}
		}

		for (Paciente paciente : listaPacientes) {
			if (paciente != null && paciente.getFechaIngreso().equals(fechaAntigua)) {
				return paciente;
			}
		}
		return null;

	}

	public void ere() {

		Iterator<Paciente> iteratorPacientes = listaPacientes.iterator();

		while (iteratorPacientes.hasNext()) {

			Paciente pacienteActual = iteratorPacientes.next();

			boolean estaEnMedicacion = false;

			for (Medicacion medicacion : listaMedicacion) {

				if (medicacion.getPacienteMedicacion() != null
						&& medicacion.getPacienteMedicacion().getEnfermedad().equals(pacienteActual.getEnfermedad())) {

					estaEnMedicacion = true;

				}

			}

			if (!estaEnMedicacion) {

				iteratorPacientes.remove();

			}

		}

	}

}
